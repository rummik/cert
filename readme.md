AmaziCert [![Builds][]][travis] [![Deps][]][gemnasium] [![Donations][]][gittip]
=========
Certificate authorities are always charging a lot.  Not sure why.  Let's try to
make this a little more freedom oriented, because security should be free.

[Builds]: http://img.shields.io/travis-ci/rummik/cert.png "Build Status"
[travis]: https://travis-ci.org/rummik/cert
[Deps]: https://gemnasium.com/rummik/cert.png "Dependency Status"
[gemnasium]: https://gemnasium.com/rummik/cert
[Donations]: http://img.shields.io/gittip/rummik.png
[gittip]: https://www.gittip.com/rummik/


## Getting Started
_(Coming soon)_


## Documentation
_(Coming soon)_


## Examples
_(Coming soon)_


## Contributing
Please see the [Chameleoid Styleguide][] before contributing.

Take care to maintain the existing coding style.  Add unit tests for any new or
changed functionality.  Lint and test your code using [Grunt][].

[Chameleoid Styleguide]: https://github.com/chameleoid/style
[Grunt]: http://gruntjs.com/


## Release History
_(Nothing yet)_


## License
Copyright (c) 2014
Licensed under the MPL license.
